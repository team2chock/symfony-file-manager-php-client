# File

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**url** | **string** |  | [optional] 
**extension** | **string** |  | [optional] 
**mime_type** | **string** |  | [optional] 
**oauth_client** | [**\Swagger\Client\Model\Client**](Client.md) |  | [optional] 
**created** | [**\DateTime**](\DateTime.md) |  | [optional] 
**updated** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

